﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Cursov.models;
using Xamarin.Essentials;

namespace Cursov
{
    public class RestService
    {
        HttpClient client;

        public RestService()
        {
            HttpClientHandler clientHandler = new HttpClientHandler
            {
                ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; }
            };
            client = new HttpClient(clientHandler);
        }

        JsonSerializerOptions options = new JsonSerializerOptions
        {
            PropertyNameCaseInsensitive = true,
        };
        public async Task<Response<Auth>> Auth(string login, string password)
        {
            string uri = "http://10.0.2.2:8080/user/login";
            UserAuth user = new UserAuth
            {
                login = login,
                password = password
            };
            string json = JsonSerializer.Serialize<UserAuth>(user, options);
            StringContent body = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync(uri, body);
            string content = await response.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<Response<Auth>>(content, options);
        }
        public async Task<Response<UserAuth>> Register(string login, string password)
        {
            string uri = "http://10.0.2.2:8080/user/register";
            UserAuth user = new UserAuth
            {
                login = login,
                password = password
            };
            string json = JsonSerializer.Serialize<UserAuth>(user, options);
            StringContent body = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync(uri, body);
            string content = await response.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<Response<UserAuth>>(content, options);
        }

        public async Task<Response<List<Item>>> GetCatalog()
        {
            string token = await SecureStorage.GetAsync("token");

            client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", token);

            string uri = "http://10.0.2.2:8080/catalog";
            HttpResponseMessage response = await client.GetAsync(uri);
            string content = await response.Content.ReadAsStringAsync();

            return JsonSerializer.Deserialize<Response<List<Item>>>(content, options);
        }

        public async Task<Response<List<Category>>> GetCategory()
        {
            string token = await SecureStorage.GetAsync("token");

            client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", token);
            string uri = "http://10.0.2.2:8080/catalog/category";
            HttpResponseMessage response = await client.GetAsync(uri);
            string content = await response.Content.ReadAsStringAsync();

            return JsonSerializer.Deserialize<Response<List<Category>>>(content, options);
        }

        public async Task<Response<Item>> CreateItem(Item item)
        {
            string token = await SecureStorage.GetAsync("token");

            client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", token);
            string uri = "http://10.0.2.2:8080/catalog";
            string json = JsonSerializer.Serialize<Item>(item, options);
            StringContent body = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync(uri, body);
            string content = await response.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<Response<Item>>(content, options); ;
        }
        public async Task<Response<Item>> UpdateItem(Item item)
        {
            string token = await SecureStorage.GetAsync("token");

            client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", token);
            int id = item.ID;
            string uri = $"http://10.0.2.2:8080/catalog/{id}";
            string json = JsonSerializer.Serialize<Item>(item, options);
            StringContent body = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync(uri, body);
            string content = await response.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<Response<Item>>(content, options); ;
        }
        public async Task<Response<object>> DeleteItem(int id)
        {
            string token = await SecureStorage.GetAsync("token");

            client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", token);
            string uri = $"http://10.0.2.2:8080/catalog/{id}";
            HttpResponseMessage response = await client.DeleteAsync(uri);
            string content = await response.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<Response<object>>(content, options);
        }
        public async Task<Response<List<Order>>> GetOrder()
        {
            string token = await SecureStorage.GetAsync("token");

            client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", token);

            string uri = "http://10.0.2.2:8080/order";
            HttpResponseMessage response = await client.GetAsync(uri);
            string content = await response.Content.ReadAsStringAsync();

            Console.WriteLine("GetOrder content {0}", content);

            return JsonSerializer.Deserialize<Response<List<Order>>>(content, options);
        }
        public async Task<Response<Order>> AddOrder(Item item)
        {
            string token = await SecureStorage.GetAsync("token");

            client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", token);

            //int id = item.ID;
            string uri = $"http://10.0.2.2:8080/order";
            string json = JsonSerializer.Serialize<Item>(item, options);
            StringContent body = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync(uri, body);
            string content = await response.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<Response<Order>>(content, options); ;
        }
    }
}

