﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Cursov
{
    public partial class App : Application
    {
        private static RestService apiClient;
        public static RestService ApiClient
        {
            get
            {
                if (apiClient == null)
                {
                    apiClient = new RestService();
                }
                return apiClient;
            }
        }
        public App ()
        {
            InitializeComponent();
           

            MainPage = new NavigationPage(new SignIn());
        }

        protected override void OnStart ()
        {
        }

        protected override void OnSleep ()
        {
        }

        protected override void OnResume ()
        {
        }
    }
}

