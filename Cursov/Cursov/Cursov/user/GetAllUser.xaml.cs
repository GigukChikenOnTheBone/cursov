﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Cursov.user
{	
	public partial class GetAllUser : ContentPage
	{
        models.Item bas_item;
        private List<models.Item> catalog;
        public List<models.Item> Catalog
        {
            get
            {
                return catalog;
            }
            set
            {
                catalog = value;
            }
        }
        public GetAllUser ()
		{
			InitializeComponent ();
            GetItems();
        }
        private async void GetItems()
        {
            models.Response<List<models.Item>> Response = await App.ApiClient.GetCatalog();
            Catalog = Response.Data;
            itemsCollection.ItemsSource = Catalog;
        }
        void CheckShoes(object sender, CheckedChangedEventArgs e)
        {
            itemsCollection.ItemsSource = Catalog.FindAll(item => item.Category.ID == 1);
        }
        void CheckOuterwear(object sender, CheckedChangedEventArgs e)
        {
            itemsCollection.ItemsSource = Catalog.FindAll(item => item.Category.ID == 2);
        }
        void CheckUndergarments(object sender, CheckedChangedEventArgs e)
        {
            itemsCollection.ItemsSource = Catalog.FindAll(item => item.Category.ID == 4);
        }
        void CheckHats(object sender, CheckedChangedEventArgs e)
        {
            itemsCollection.ItemsSource = Catalog.FindAll(item => item.Category.ID == 3);
        }
        void CheckAccessories(object sender, CheckedChangedEventArgs e)
        {
            itemsCollection.ItemsSource = Catalog.FindAll(item => item.Category.ID == 6);
        }
        void CheckAll(object sender, CheckedChangedEventArgs e)
        {
            itemsCollection.ItemsSource = Catalog;
        }
        private async void Buy(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            button.Text = "Товар заказан";
            var ID = (int)button.BindingContext;
            models.Item Item = catalog.Find(item => item.ID == ID);
            await App.ApiClient.AddOrder(Item);
            Item.Count -= 1;
            await App.ApiClient.UpdateItem(Item);
        }
    }
}

