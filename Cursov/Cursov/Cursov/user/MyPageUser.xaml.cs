﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Cursov.user
{	
	public partial class MyPageUser : ContentPage
	{	
		public MyPageUser ()
		{
			InitializeComponent ();
		}
        private async void GetAll_Button(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new GetAllUser());
        }
        private async void Basket_Button(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new OrderPage());
        }
        private async void ExitButtom(object sender, EventArgs e)
        {
            Navigation.InsertPageBefore(new SignIn(), this);
            await Navigation.PopAsync();
        }
    }
}

