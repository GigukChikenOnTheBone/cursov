﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Cursov.admin
{	
	public partial class MainPageAdmin : ContentPage
	{	
		public MainPageAdmin ()
		{
			InitializeComponent ();
		}
        private async void AddItemButtom(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new AddItem());
        }
        private async void GetAll_Button(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new GetAllAdmin());
        }
        private async void ExitButtom(object sender, EventArgs e)
        {
            Navigation.InsertPageBefore(new SignIn(), this);
            await Navigation.PopAsync();
        }
    }
}

