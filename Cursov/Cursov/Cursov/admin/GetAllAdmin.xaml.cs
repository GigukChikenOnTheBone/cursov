﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Cursov.admin
{
    public partial class GetAllAdmin : ContentPage
    {
        models.Item bas_item;
        private List<models.Item> catalog;
        public GetAllAdmin()
        {
            InitializeComponent();
            GetItems();
        }
        public async void GetItems()
        {
            models.Response<List<models.Item>> Response = await App.ApiClient.GetCatalog();
            catalog = Response.Data;
            itemsCollection.ItemsSource = catalog;
        }
        private  void Update_Button(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            var ID = (int)button.BindingContext;
            models.Item Item = catalog.Find(item => item.ID == ID);
            Navigation.PushAsync(new admin.Update(Item));
        }
        private async void Delete_Button(object sender, EventArgs e)
        {
            Button item = (Button)sender;
            var ID = (int)item.BindingContext;
            await App.ApiClient.DeleteItem(ID);
            GetItems();
          

        }
        void CheckShoes(object sender, CheckedChangedEventArgs e)
        {
            itemsCollection.ItemsSource = catalog.FindAll(item => item.Category.ID == 1);
        }
        void CheckOuterwear(object sender, CheckedChangedEventArgs e)
        {
            itemsCollection.ItemsSource = catalog.FindAll(item => item.Category.ID == 2);
        }
        void CheckUndergarments(object sender, CheckedChangedEventArgs e)
        {
            itemsCollection.ItemsSource = catalog.FindAll(item => item.Category.ID == 4);
        }
        void CheckHats(object sender, CheckedChangedEventArgs e)
        {
            itemsCollection.ItemsSource = catalog.FindAll(item => item.Category.ID == 3);
        }
        void CheckAccessories(object sender, CheckedChangedEventArgs e)
        {
            itemsCollection.ItemsSource = catalog.FindAll(item => item.Category.ID == 6);
        }
        void CheckAll(object sender, CheckedChangedEventArgs e)
        {
            itemsCollection.ItemsSource = catalog;
        }
    }
}

