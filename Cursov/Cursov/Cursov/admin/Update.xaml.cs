﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Cursov.admin
{
    public partial class Update : ContentPage
    {
        private models.Category categoriesid;
        private List<models.Category> categories;
        public List<models.Item> catalog;
        public models.Item update_item = new models.Item { };

        public Update(models.Item item)
        {
            InitializeComponent();
            update_item = item;
            GetCategory(item);
            articleFelder.Text = Convert.ToString(item.Articul);
            titleFelder.Text = item.Title;
            descFilder.Text = item.Des;
            imageFelder.Text = item.Url_Image;
            quantityFelder.Text = Convert.ToString(item.Count);
            priceFelder.Text = Convert.ToString(item.Price);
        }
        private async void GetCategory(models.Item item)
        {
            models.Response<List<models.Category>> response = await App.ApiClient.GetCategory();
            categories = response.Data;
            foreach (models.Category category in response.Data)
            {
                picker.Items.Add(category.Name);
            }
            picker.SelectedIndex = categories.FindIndex(category => category.ID == item.Category.ID);
            categoriesid = categories.Find(category => category.ID==item.Category.ID);
        }
        void picker_SelectedIndexChanged(object sender, EventArgs e)
        {
            int ID = categories[picker.SelectedIndex].ID;
            string name = picker.Items[picker.SelectedIndex];            
            models.Category category = new models.Category
            {
                ID = ID,
                Name = name,
            };
            categoriesid = category;

        }
        private async void UpdateItemButon(object sender, EventArgs e)
        {
            update_item.Articul = Convert.ToInt32(articleFelder.Text.Trim());
            update_item.Title = titleFelder.Text.Trim();
            update_item.Category = categoriesid;
            update_item.Des = descFilder.Text.Trim();
            update_item.Url_Image = imageFelder.Text.Trim();
            update_item.Count = Convert.ToInt32(quantityFelder.Text.Trim());
            update_item.Price = Convert.ToInt32(priceFelder.Text.Trim());
            await App.ApiClient.UpdateItem(update_item);
            Navigation.PopToRootAsync();
        }
    }
}

