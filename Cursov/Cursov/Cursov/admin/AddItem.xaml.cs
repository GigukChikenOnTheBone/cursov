﻿using System;
using System.Collections.Generic;
using Cursov.models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Cursov.admin
{
    
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddItem : ContentPage
    {
        public models.Category categoriesid;
        public List<models.Category> categories;

        public AddItem()
        {
            InitializeComponent();

            GetCategory();
        }

        private async void GetCategory()
        {
            models.Response<List<models.Category>> response = await App.ApiClient.GetCategory();
            categories = response.Data;
            foreach (models.Category category in response.Data)
            {
                picker.Items.Add(category.Name);
            }
        }

        void picker_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (picker.SelectedIndex == -1)
            {
                return;
            }

            int ID = categories[picker.SelectedIndex].ID;
            string name = picker.Items[picker.SelectedIndex];
            Console.WriteLine("Category {0}, {1}", name, ID);
            models.Category category = new models.Category
            {
                ID = ID,
                Name = name,
            };
            categoriesid = category;
            
        }

        private async void AddItemButtom(object sender, EventArgs e)
        {
            int article = Convert.ToInt32(articleFelder.Text.Trim());
            string title = titleFelder.Text.Trim();
            string desc = descFilder.Text.Trim();
            string image = imageFelder.Text.Trim();
            int quantity = Convert.ToInt32(quantityFelder.Text.Trim());
            int price = Convert.ToInt32(priceFelder.Text.Trim());
            if (article < 5)
            {
                await DisplayAlert("Error", "Title min 5", "Ok");
                return;
            }
            else if (title.Length < 5)
            {
                await DisplayAlert("Error", "Title min 5", "Ok");
                return;
            }
            else if (desc.Length < 10)
            {
                await DisplayAlert("Error", "Desc min 10", "Ok");
                return;
            }
            else if (image.Length < 15)
            {
                await DisplayAlert("Error", "Image min 15", "Ok");
                return;
            }
            else if (quantity < 0)
            {
                await DisplayAlert("Error", "Quantity min 1", "Ok");
                return;
            }
            else if (price < 20)
            {
                await DisplayAlert("Error", "Quantity min 20", "Ok");
                return;
            }
            else
            {
                models.Item item = new models.Item
                {
                    Articul = article,
                    Title = title,
                    Category = categoriesid,
                    Des = desc,
                    Url_Image = image,
                    Count = quantity,
                    Price = price
                };
                await App.ApiClient.CreateItem(item);


                articleFelder.Text = "";
                titleFelder.Text = "";
                categoriesid = null;
                picker.SelectedIndex = -1;
                descFilder.Text = "";
                imageFelder.Text = "";
                quantityFelder.Text = "";
                priceFelder.Text = "";
            }
        }
    }
}

