﻿namespace Cursov.models
{
    public class Item
    {
        // id Товара
        public int ID { get; set; }
        // Артикул товара
        public int Articul { get; set; }
        // Название товара
        public string Title { get; set; }
        // Описание Категория
        public Category Category { get; set; }
        // Описание товара
        public string Des { get; set; }
        // Изображение товара (URL)
        public string Url_Image { get; set; }
        // Количество товара
        public int Count { get; set; }
        // Цена товара
        public int Price { get; set; }

    }
}

