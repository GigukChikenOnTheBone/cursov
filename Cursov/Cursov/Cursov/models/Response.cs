﻿using System;
using System.Text.Json.Serialization;
using Xamarin.Forms;

namespace Cursov.models
{
    public class Response<T>
    {

        // Статус
        public bool Success { get; set; }
        // Сообщение
        public string Message { get; set; }
        // Данные
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public T Data { get; set; }
    }
}

