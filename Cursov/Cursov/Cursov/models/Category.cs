﻿using System.Collections.Generic;

namespace Cursov.models
{
    public class Category
    {
        // id 
        public int ID { get; set; }
        // Название категории
        public string Name { get; set; }
    }
}