﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Essentials;

namespace Cursov
{
    public partial class OrderPage : ContentPage
    {
        public List<models.Order> order;
        public OrderPage()
        {
            InitializeComponent();
            GetOrders();
        }

        public async void GetOrders()
        {
            models.Response<List<models.Order>> Response = await App.ApiClient.GetOrder();
            order = Response.Data;
            Console.WriteLine("Date sos{0}", order);
            orderCollection.ItemsSource = order;
        }
    }
}

