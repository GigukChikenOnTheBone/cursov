﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Essentials;
using System.Threading;

namespace Cursov
{
    public partial class SignIn : ContentPage
    {
        public SignIn()
        {
            InitializeComponent();
        }

        private async void MainPage(object sender, EventArgs e)
        {
            string login = Login.Text != null ? Login.Text.Trim() : "";
            string password = Password.Text != null ? Password.Text.Trim() : "";
            models.Response<models.Auth> data = await App.ApiClient.Auth(login, password);

            if (data.Success == false)
            {
                string messeng = data.Message;
                await DisplayAlert("Ошибка", messeng, "Ok");
                return;
            }

            await SecureStorage.SetAsync("token", data.Data.Token);
            await SecureStorage.SetAsync("role", data.Data.Role_Id.ToString());

            if (data.Data.Role_Id == 2)
            {
                Navigation.InsertPageBefore(new user.MyPageUser(), this);

            }
            else
            {
                Navigation.InsertPageBefore(new admin.MainPageAdmin(), this);
            }

            await Navigation.PopAsync();
        }
        private async void RegisterPage(object sender, EventArgs e)
        { 
            await Navigation.PushAsync(new RegisterPage()); 
        }
    }

}

