﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Xamarin.Forms;

namespace Cursov
{	
	public partial class RegisterPage : ContentPage
	{	
		public RegisterPage ()
		{
			InitializeComponent ();
            
        }
        private async void RegisterButton(object sender, EventArgs e)
        {
            string login = loginFelder.Text.Trim();
            string password = passwordFelder.Text.Trim();
            if (login.Length<5)
            {
                await DisplayAlert("Ошибка", "Логин должен быть более 5 символов", "Ok");
                return;
            }
            else if (password.Length < 5)
            {
                await DisplayAlert("Ошибка", "Пароль должен быть более 5 символов", "Ok");
                return;
            }
            models.Response<models.UserAuth> data = await App.ApiClient.Register(login, password);
            if (data.Success == false)
            {
                string messeng = data.Message;
                await DisplayAlert("Ошибка", messeng, "Ok");
                return;
            }
            string mes = data.Message;
            await DisplayAlert("Всё ок", mes, "Ok");
        }
    }
}

