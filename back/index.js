const express = require('express');
const app = express();
const catalogRoute = require('./routes/catalog')
const userRoute = require('./routes/user')
const orderRoute = require('./routes/order')
app.listen(8080, () =>console.log("server good port: 8080"));

app.use('/catalog', catalogRoute);
app.use('/user', userRoute);
app.use('/order', orderRoute);



