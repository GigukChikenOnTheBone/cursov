const express = require('express');
const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken')

const router = express.Router();
const auth = require('../auth');
const { knex } = require('../config/databaseConfig');

router.get('/', auth, async (req, res) => {
    const id = req.users;

    const user = await knex('user').select('*').where('id', id).first();

    res.json({
        success: true,
        data: user,
    });
})

router.post('/login', express.json(), async (req, res) => {
    try {
        const {login, password} = req.body;

        if (!login || !password) {
            return res.send({
                success: false,
                message: "Логин или пароль не введны",
                data: null,
            });
        } else {
            const user = await knex('user').select('*').where('login', login).first();
            if (user) {
                const comparePassword = await bcryptjs.compare(password, user.password);

                if (comparePassword) {
                    const token = jwt.sign(user.id, "jgvchgvfkuycvjhvkcuvwljchvyucvhj")

                    return res.send({
                        success: true,
                        message: "Авторизация прошла успешно",
                        data: {
                            token,
                            id: user.id,
                            role_id: user.role_id
                        }
                    })
                } else {
                    return res.send({
                        success: false,
                        message: "Неверный логин или пароль",
                        data: null,
                    })
                }
            }
            else{
                return res.send({
                    success: false,
                    message: "Неверный логин или пароль",
                    data: null,
                })
            }
        }
    } catch (error) {
        console.log('error login', error)
    }
})

router.post('/register', express.json(), async (req, res) => {
    try {
        const { login, password } = req.body;
        if (!login || !password) {
            return res.send({
                success: false,
                message: 'Пожалуйста введите ваши данные',
                data: null,
            })
        }
        
        const user = await knex('user').select('*').where('login', login).first();

        if (user) {
            return res.send({
                success: false,
                message: 'Этот login уже занят',
                data: null,
            })
        }
            
        const hash_password = await bcryptjs.hash(password, 12);
        const result = await knex('user').insert({
            login,
            password: hash_password,
            role_id: 2,
        });

        const user_id = { 
            user_id: result,
        };
        const token = jwt.sign(user_id, "jgvchgvfkuycvjhvkcuvwljchvyucvhj");

        if (result) {
            return res.send({
                success: true,
                message: 'Регестрация прошла успешно',
                data: {
                    token: token,
                }
            }) 
        }

        return res.send({
            success: false,
            message: 'Произошла ошибка',
            data: null,
        });
    } catch (error) {
        return res.send({
            success: false,
            message: error.message,
            data: null,
        })

    }
});

module.exports = router;
