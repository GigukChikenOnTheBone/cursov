const express = require('express');
const { knex } = require('../config/databaseConfig');
const date = require('date-fns');
const rulocale = require('date-fns/locale/ru')
const auth = require('../auth');
const router = express.Router();

router.get('/', auth, async (req,res)=>{
    const order = await knex('order').select('*').where('user_id', req.user_id);

    const result = [];

    for (let i = 0; i < order.length; i++) {
        const catalog = await knex('catalog').select('*').where('id', order[i].catalog_id);
        const category = await knex('Category').select('*').where('id', catalog[0].category_id);

        delete catalog[0].category_id;

        const newCatalog = {
            ...catalog[0],
            category: category[0],
        }
        
        result.push({
            id: order[i].id,
            catalog: newCatalog,
            date: date.format(order[i].date,'PPP',{locale:rulocale})
        })
    }

    res.send({
        success: true,
        message: "",
        data: result,
    })
})

// add order
router.post('/',auth, express.json(), async (req,res)=>{
    const body = req.body;
    const id =req.user_id;
    await knex('order').insert({
        user_id: id,
        catalog_id: body.ID
    });
    res.send({
        success: true,
        message: "Товар куплен",
        data: null,
    })

    
})
module.exports=router;