const express = require('express');
const { knex } = require('../config/databaseConfig');
const auth = require('../auth');
const router = express.Router();

router.get('/', auth, async (req,res)=>{
    const catalog = await knex('catalog').select('*').where('count', ">", 0);

    const result = [];
    
    for (let i = 0; i < catalog.length; i++) {
        const category = await knex('Category').select('*').where('id', catalog[i].category_id);

        delete catalog[i].category_id; 

        result.push({
            ...catalog[i],
            category: category[0],
        });
    }

    return res.json({
        success: true,
        message: null,
        data: result,
    });
})

//update
router.post('/:id', auth, express.json(), async (req,res)=>{
        const id = req.params.id;
        const param = req.body;
        if (id>0 ){
            knex('catalog')
            .where('id','=', id)
            .update({
                articul: param.Articul,
                title: param.Title,
                des: param.Des,
                url_image: param.Url_Image,
                count: param.Count,
                price: param.Price,
                category_id: param.Category.ID
            })
            .then(()=>{
                res.json({
                    success: true,
                    message: 'Товар успешно обновлен',
                    data: null
                });
            }).catch(error =>{
                res.json({
                    successe: false,
                    message: error.message,
                    data: null,

                })
            })
            
        }
        else {
            res.status(404)
            res.send({error: "Такого товара не существует"})
        }
    })


router.delete('/:id', auth, (req,res) => { 
    const id = req.params.id;
    console.log('id', id, req.params);
    knex('catalog').where('id',id).del().then(()=> {
        console.log('delete success');
        return res.json({
            success: true,
            message: 'Товар успешно удалён',
            data: null
        });
    });


})

// create
router.post('/', auth, express.json(), async (req,res)=>{
    const param = req.body;

    await knex('catalog').insert({
        articul: param.Articul,
        title: param.Title,
        des: param.Des,
        url_image: param.Url_Image,
        count: param.Count,
        price: param.Price,
        category_id: param.Category.ID
    });

    res.json({
        success: true,
        message: "Товар добавлен",
        data: null,
    });
})

router.get('/category', auth, async (req, res) => {
    const category = await knex('Category').select('*');

    res.send({
        success: true,
        message: "",
        data: category,
    })
})

module.exports=router;
