const knex = require('knex')({
    client: 'mysql',
    connection: {
        host:"localhost", 
        port:'3306',
        user:'lab3',
        password:'1234',
        database:'shop',
        multipleStatements: true,
    }
});

module.exports={
    knex,
}